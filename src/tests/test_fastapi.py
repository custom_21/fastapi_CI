from src.fastapi_app.main import app
from fastapi.testclient import TestClient

client = TestClient(app)

def test_read_main():
    response = client.get("/")
    assert response.status_code == 404

def test_read_main():
    response = client.get("/recipes")
    assert response.status_code == 200
