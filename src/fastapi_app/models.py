from sqlalchemy import Column, Integer, String

from database import Base


class Recipe(Base):

    __tablename__ = 'Recipe'
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(60), nullable=False)
    description = Column(String(140))
    ingredients = Column(String(300), nullable=False)
    views = Column(Integer, default=0)
    cooking_time = Column(Integer, nullable=False)

