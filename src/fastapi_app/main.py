from fastapi import FastAPI, Request, Path
from sqlalchemy.future import select
import models
import schemas
from database import engine, session
from typing import List
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")

get_recipe_info_by_id_sql = """
SELECT *
FROM Recipe
WHERE id 
LIKE ?;
"""


@app.on_event('startup')
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event('shutdown')
async def shutdown():
    await session.close()
    await engine.dispose()


@app.get('/recipes', response_model=List[schemas.RecipeOut])
async def get_all_recipes(request: Request):
    res = await session.execute(select(models.Recipe))
    # print(res.scalars().all())
    sorted_res = sorted(res.scalars().all(), key=lambda x: x.views, reverse=True)
    return templates.TemplateResponse("index.html", {"request": request, 'recipes': sorted_res})


@app.get('/recipes/{id}')
async def get_recipe(
        id: int = Path(
            ...,
            title='Recipe id',

        )
):
    res = await session.execute(f"SELECT * FROM Recipe WHERE id = {id}")
    await session.execute(f'UPDATE Recipe SET views = views + 1 WHERE id = {id}')
    await session.commit()
    return res.fetchone()

def create_app():
    app = FastAPI()
    return app
