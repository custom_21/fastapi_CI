from pydantic import BaseModel


class BaseRecipe(BaseModel):
    name: str
    views: int
    description: str
    ingredients: str
    cooking_time: int


class RecipeIn(BaseRecipe):
    ...


class RecipeOut(BaseRecipe):
    id: int

    class Config:
        orm_model = True
